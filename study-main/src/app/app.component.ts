import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationComponent } from './authentication/authentication.component';
import { UsersService } from './users.service';
import { TestService } from './test.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [TestService],
})
export class AppComponent {
  userName = '';
  users: any;
  displayedColumns: string[] = ['firstname', 'lastname'];

  constructor(private testService: TestService, private http: HttpClient) {
  }

  getAllUsers(): void {


      this.http.get( 'https://fakestoreapi.com/users'  )
      .subscribe((response) => {
        this.users = response;
      });

  }
  // getAllUsers(): void {
  //   this.users = this.testService.getAllUsers() as Observable<User>[];
  // }
}

interface User {
  name: UserName;
}

interface UserName {
  firstname: string;
  lastname: string;
}
