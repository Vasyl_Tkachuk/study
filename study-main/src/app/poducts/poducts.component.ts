import { Component, OnInit } from '@angular/core';
import {Item} from 'src/app/item';

@Component({
  selector: 'app-poducts',
  templateUrl: './poducts.component.html',
  styleUrls: ['./poducts.component.scss']
})

export class PoductsComponent {
  text: string = "";
  price: number = 0;
   
  items: Item[] = 
  [
      { purchase: "Хлеб", id: 1, done: false, price: 15.9},
      { purchase: "Масло", id: 2, done: false, price: 60},
      { purchase: "Картофель", id: 3, done: false, price: 22.6},
      { purchase: "Сыр", id: 4, done: false, price:310}
  ];
  addItem(text: string, price: number): void {
       
      if(text==null || text.trim()=="" || price==null) {
          return;
      }
      this.items.push(new Item(text, price, Math.max.apply(Math, this.items.map(function(o) { return o.id; }))+1));
  }
}
