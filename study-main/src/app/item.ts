export class Item{
    id: number;
    purchase: string;
    done: boolean;
    price: number;
     
    constructor(purchase: string, price: number, id: number) {
        this.id = id;
        this.purchase = purchase;
        this.price = price;
        this.done = false;
    }
}