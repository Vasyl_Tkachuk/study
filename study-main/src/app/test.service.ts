import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) { }

  getAllUsers(): any {

      let result: any;

      this.http.get( 'https://fakestoreapi.com/users'  )
      .subscribe((response) => {
        result = response;
      });

      return result;
  }

  getAllUsersById(id: string): any {

    let result: any;

    this.http.get( 'https://fakestoreapi.com/users/' + id )
    .subscribe((response) => {
      result = response;
    });

    return result;
}
}
