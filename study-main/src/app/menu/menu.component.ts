import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { LoginButtonComponent } from './../login-button/login-button.component';
import { AuthenticationComponent } from '../authentication/authentication.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent{

  constructor(public dialog: MatDialog) {}

  openRegister(): void {
    this.dialog.open(AuthenticationComponent);
  }


  openLogin(): void {
    this.dialog.open(LoginButtonComponent);
  }
}
