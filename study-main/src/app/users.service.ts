import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }

  public getUsers(): Array<any>{
    return[
      {firstname: 'Ivan', lastname: 'Ivanov'},
      {firstname: 'Ivan', lastname: 'Ivanov'},
      {firstname: 'Ivan', lastname: 'Ivanov'},
    ];
  }
}
