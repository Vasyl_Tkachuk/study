import { Component, OnInit } from '@angular/core';
import { Item } from 'src/app/item';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  constructor(private activateRoute: ActivatedRoute) {
    this.items =
      [
        { purchase: 'Хлеб', id: 1, done: false, price: 15.9 },
        { purchase: 'Масло', id: 2, done: false, price: 60 },
        { purchase: 'Картофель', id: 3, done: false, price: 22.6 },
        { purchase: 'Сир', id: 4, done: false, price: 310 }
      ];
    // tslint:disable-next-line:no-string-literal
    this.curentItem = this.items.find(p => p.id == this.activateRoute.snapshot.params['id']) as Item;
  }

  items: Item[];
  curentItem: Item;
  ngOnInit(): void {

  }
}