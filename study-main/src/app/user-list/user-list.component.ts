import { UsersService } from './../users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  public users: Array<any>;

  constructor(private usersService: UsersService) {
    this.users = usersService.getUsers();
   }

  ngOnInit(): void {
  }

}
