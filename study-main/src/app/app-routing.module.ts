import { LoginButtonComponent } from './login-button/login-button.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { PoductsComponent } from './poducts/poducts.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'products' },
  { path: 'products', component: PoductsComponent },
  { path: 'products/:id', component: ProductComponent },
  { path: 'login-button', component: LoginButtonComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
